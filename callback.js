// const { rejects } = require('assert');
// const fs = require('fs');
// const { resolve } = require('path');

// fs.readFile(' writeFile(file:')

// fs.readFile('test.txt', {encoding: 'utf-8'} (err, data) => {
//     //do processs
//     if (!err) {
//         console.log('kedua');
//         console.log(data);
//     }
// });

// let student = [
//     {
//         'name' : 'sabrina'
//     },
//     {
//         'name' : 'sabrina'
//     }
// ];

// fs.writeFile('test-tulis.json', JSON.stringify(student), {encoding: 'utf-8'}, () => {
//     console.log('berhasil');
// });

// let data = fs.readFileSync('test.txt', {encoding: 'utf-8'});
// console.log('pertama');
// console.log(data);
// console.log('kedua');

// fs.writeFileSync('test-tulis.json', JSON.stringify(student), {encoding: 'utf-8'});



//set time out
// console.log('pertama');
// setTimeout(() => {console.log('kedua');}, 5000);
// console.log('ketiga');

// //set interval langsung handler
// let i = 0;
// setInterval(() => {console.log('cetak ke-', i);}, 5000);

// let dataSiswa = ['sabrina', 'fikri'];
// function cetakJumlahKata(arr, callback) {
//     let newArr = [];

//     arr.forEach((el) => {
//         newArr.push();
//     });
//     return newArr;
// }

// let hasil = cetakJumlahKata(dataSiswa, (kata) => {
//     return kata.lenght;
// });

// console.log(hasil);

//PROMISE
const newPromise = new Promise((resolve) => resolve('berhasil'));

// newPromise
//     .then(data => console.log(data)) //berhasil
//     .catch(err => console.log(err)); //error

// function isPasswordCorrect(password) {
//     return new Promise((resolve, rejects) => {
//         if(password === 'password123') {
//             resolve('password benar');
//         } else {
//             rejects('password salah');
//         }
//     });
// }

//bisa juga pake
// function isPasswordCorrect(password) {
//     return new Promise((resolve, rejects) => {
//         if(password === 'password123') {
//             resolve({
//                 err:'',
//                 data: 'password benar'
//             });
//         } else {
//             rejects({
//                 err: 'password salah'
//             });
//         }
//     });
// }

//menggunakan then catch
// isPasswordCorrect('password')
//     .then(data => console.log(data))
//     .catch(err => console.log(err));

//menggunakan async await
// async function main() {
//     try {
//         let hasil =  await isPasswordCorrect('password123');
//         console.log(hasil);
//     } catch(err) {
//         console.log(err.massage);
//     }
// }

//yang resolvenya pake
// async function main() {
//     try {
//         let hasil = await isPasswordCorrect('password123');
//         if(hasil.err) {
//             console.log('errornya adalah', hasil.err);
//         }
//     } catch(err) {
//         console.log(err);
//     }
// }
// main();

//classs
class Human {
    //static  property
    static isLivingOnEarth = true;

    //static method
    static walk() {
        return 'sedang berjalan';
    }

    //instance property
    constructor(name, address) {
        this.name = name; //buat manggil class
        this.address = address;
        this.password = this.password;
    }

    //introduce 
    introduce() {
        return 'hello my name is ' + this.name;
    }

    checkPassword(password) {
        if(password == this.password) {
            return 'password benar';
        } else {
            return 'password salah';
        }
    }
};
//add new static meethod
Human.lunch = () => {
    return 'having some lunch';
};
//add new instance method
Human.prototype.greeting = (name) => {
    return 'Hi ${name}, my name is ${this.name}';
};

// access static method
console.log(Human.lunch());
console.log(Human.isLivingOnEarth);
console.log(Human.walk());

//initiate new human
let student1 = new Human('sabrina', 'jakarta', 'password123');

//access instance property and method
console.log(student1.name);
console.log(student1.introduce());
console.log(student1.checkPassword());
console.log(student1.greeting('jhon'));