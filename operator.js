//operator//

// let x = "10";
// console.log(typeof x);
// x = -x//
// console.log(typeof x);
// console.log(x);

// let x = 10;
// let y = 10;
// let z = 10;
// console.log(x+y+z);

//increment decrement
// let a = 10;
// console.log(a++); //harus direturn//
// console.log(a); hasilnya 10

// let a = 10;
// console.log(a++);//langsungan// hasilnya 11

//modify
// let totNilai = 0;
// totNilai = totNilai+5;
// totNilai = totNilai+10;
// console.log(totNilai);

// console.log(5 > 1 || 2 < 3);

//function declaration
// function hitungdiskon(harga, diskon) {
//     let hasil = harga * (diskon / 100);
//     return hasil;
// }
// console.log(hitungdiskon(100000, 10));
// function cetaknama(daftarnama) {
//     daftarnama.forEach(function (nama) {
//         console.log(nama);
//     });
// }

// cetaknama('tantri');

// function nama(ininama) {
//     let namaku = ininama;
//     return namaku;
// }
// console.log(nama("tantri"));

//function expression
// let hitungdiskon2 = function (harga, diskon) {
//     let hasil = harga * (diskon/100);
//     return hasil;
// };
// console.log(hitungdiskon2(500000, 10));

// //arrow function pake =>
// let hitungdiskon3 = (harga, diskon) => {
//     let hasil = harga * (diskon/ 100);
//     return hasil;
// };
// console.log(hitungdiskon3(300000, 10));

// const strArray = ['js', 'golang', 'php'];

// function forEach(array, callback) {
//     const newArray = [];

//     array.forEach((data, i) => {
//         newArray.push(callback(data));
//     });
//     return newArray;
// }
// let hasil = forEach(strArray, (str) => {
//     return str.length;
// });
// console.log(hasil);
