let student = [{
    name: "sabrina",
    age: 14,
    hobby: ['swimming', 'running']
}, {
    name: "agung",
    age: 15,
    hobby: ['running', 'reading']
}, {
    name: "alex",
    age: 17,
    hobby: ['reading', 'running']
}];

let newStudent = student.map(function(student){

    let gender;
    if (student.gender == 'l') {
        gender = 'Mr. ';
    } else {
        gender = 'Ms. ';
    }
    return {
        name: 'Mr. ' + student.name,
        age: student.age,
        hobby: student.hobby,
    };
});

console.log(newStudent); 
console.log(student);
// let adult = [];

// student.forEach(function (student) {
//     if(student.age >= 17) {
//         adult.push(student);
//     }
// });
// console.log(adult);

// let adult2 = student.filter(function (el) {
//     return el.age >= 17;
// });
// console.log(adult2);

let newArr = ['sabrina', 'alex', 'agung'];

console.log(newArr)